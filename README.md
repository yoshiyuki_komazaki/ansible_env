# README #

```
#!shell

# Create vm
$ vagrant up

# Connect
$ vagrant ssh

# Create an image
vagrant:~$ ./build_docker

# Run the container
vagrant:~$ ./run_docker

# Test sample-playbook
vagrant:~$ ansible-playbook -i hosts sample-playbook.yml

```

## Tips  ##

```
#!shell
# Stop running containers
$ docker stop `docker ps -a -q`

# Kill docker processes
$ docker rm `docker ps -a -q`

# Remove all '<none>' images
$ docker rmi $(docker images | awk '/^<none>/ { print $3 }')
```