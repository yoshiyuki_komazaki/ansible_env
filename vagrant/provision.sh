#!/bin/bash

packages=(
  ansible
  expect
)

sudo apt-get update
sudo apt-get install -y "${packages[@]}"

ssh-keygen -t rsa -N '' -f .ssh/id_rsa
cp /vagrant/ansible/sample-playbook.yml .
cp /vagrant/docker/Dockerfile .
cp /vagrant/docker/build_docker .
cp /vagrant/docker/run_docker .

# serverspec
sudo gem install bundler
mkdir serverspec
cd serverspec
cp /vagrant/vagrant/Gemfile .
bundle install --path gems

# docker
curl -sSL https://get.docker.com/ | sh
sudo usermod -aG docker vagrant
